var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var routes = require('./routes/index');
var users = require('./routes/users');
var twilio = require('twilio');
var app = express();
var config = require('./config');
var client = new twilio.RestClient(config.accountSid, config.authToken);
var cron = require('node-cron');

// Cron ****** - seconds, minutes, hours, day of month, month, day of week
cron.schedule('*/30 * * * * *', function() {
    client.sms.messages.create({
    to: config.toPhoneNumber,
    from: config.fromPhoneNumber,
    body: "What's wrong?"
  }, (err, message) => {
    console.log(message.sid);
  });
});

module.exports = app;
