Install

1. Clone repo
2. npm install
3. Create a config file called config.js and put it in root of directory, your toPhoneNumber, accountSid, authToken need to be created on https://www.twilio.com/
	module.exports = {
	toPhoneNumber: "+12345678",
	fromPhoneNumber: "+12345679",
	accountSid: "yourAccountSid",
	authToken: "yourToken"
}
4. npm start